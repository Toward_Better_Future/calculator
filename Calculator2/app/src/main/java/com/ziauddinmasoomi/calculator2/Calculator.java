package com.ziauddinmasoomi.calculator2;

/**
 * Created by Zia Uddin Masoomi on 4/4/2017.
 */

public class Calculator {

    private double number1;
    private double number2;
    private double result;
    private char operation;


    public  Calculator type(double number){
        number1 = number2;
        number2 = number;
        return this;
    }

    public double getResult(){
        return  result;
    }

    public void reset(){
        number1 = number2 = 0;
        operation = '+';
    }

    public Calculator add(){
        operation = '+';
        return this;
    }
    public Calculator subtract(){
        operation = '-';
        return this;
    }
    public Calculator multiply(){
        operation = '*';
        return this;
    }
    public Calculator divide(){
        operation = '/';
        return this;
    }

    public Calculator equal(){
        switch (operation){
            case '+':
                result = number1 + number2;
                break;
            case '-':
                result = number1 - number2;
                break;
            case '*':
                result = number1 * number2;
                break;
            case '/':
                result = number1 / number2;
                break;
        }
        number2 = result;
        return this;
    }

}
