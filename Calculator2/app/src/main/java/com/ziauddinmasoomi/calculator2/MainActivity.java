package com.ziauddinmasoomi.calculator2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private Button num1,num2,num3,num4,num5,num6,num7,num8,num9,zero,point,equal,divide,multiple,subtract,sum;
    private ImageButton backspace;
    private TextView resultF,historF;
    private String text,history = "";
    private Calculator calculator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        num1 = (Button) findViewById(R.id.num1);
        num2 = (Button) findViewById(R.id.num2);
        num3 = (Button) findViewById(R.id.num3);
        num4 = (Button) findViewById(R.id.num4);
        num5 = (Button) findViewById(R.id.num5);
        num6 = (Button) findViewById(R.id.num6);
        num7 = (Button) findViewById(R.id.num7);
        num8 = (Button) findViewById(R.id.num8);
        num9 = (Button) findViewById(R.id.num9);
        zero = (Button) findViewById(R.id.zero);
        point = (Button) findViewById(R.id.point);
        equal = (Button) findViewById(R.id.equal);
        divide = (Button) findViewById(R.id.divide);
        multiple = (Button) findViewById(R.id.multiple);
        subtract = (Button) findViewById(R.id.subtract);
        backspace = (ImageButton) findViewById(R.id.backspace);
        sum = (Button) findViewById(R.id.sum);
        resultF = (TextView) findViewById(R.id.result);
        historF = (TextView) findViewById(R.id.history);
    }

    @Override
    protected void onStart() {
        super.onStart();
        calculator = new Calculator();
        num1.setOnClickListener(this);
        num2.setOnClickListener(this);
        num3.setOnClickListener(this);
        num4.setOnClickListener(this);
        num5.setOnClickListener(this);
        num6.setOnClickListener(this);
        num7.setOnClickListener(this);
        num8.setOnClickListener(this);
        num9.setOnClickListener(this);
        zero.setOnClickListener(this);
        point.setOnClickListener(this);

        equal.setOnClickListener(this);
        backspace.setOnClickListener(resetAll);

        sum.setOnClickListener(this);
        subtract.setOnClickListener(this);
        divide.setOnClickListener(this);
        multiple.setOnClickListener(this);


    }

    private View.OnClickListener resetAll = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ImageButton back = (ImageButton)v;

                calculator.reset();
                resultF.setText("0");
                historF.setText("");
                history = "";
        }
    };

    public void onClick(View v) {

        history += ((Button)v).getText().toString();

        if (resultF.length() == 1 && resultF.getText().charAt(0) == '0'){
            text = "";
        } else {
            text = resultF.getText().toString();
        }

        char c = ((Button)v).getText().charAt(0);

        if (Character.isDigit(c)){
            resultF.setText(text+c);
        }
        else if (c == '+'){
            calculator.type(Double.parseDouble(resultF.getText().toString()));
            calculator.add();
            resultF.setText("0");
        }
        else if (c == '-'){
            calculator.type(Double.parseDouble(resultF.getText().toString()));
            calculator.subtract();
            resultF.setText("0");
        }
        else if (c == '*'){
            calculator.type(Double.parseDouble(resultF.getText().toString()));
            calculator.multiply();
            resultF.setText("0");
        }
        else if (c == '/'){
            calculator.type(Double.parseDouble(resultF.getText().toString()));
            calculator.divide();
            resultF.setText("0");
        }
        else if (c == '.'){
           if (!resultF.getText().toString().contains(".")){
               if (resultF.length() == 1 && resultF.getText().charAt(0) == '0'){
                   text = "0";
                   resultF.setText(text+c);
               } else {
                  resultF.setText(text+c);
               }
           }
        }
        else if (c == '='){
            calculator.type(Double.parseDouble(resultF.getText().toString()));
            calculator.equal();

            if (calculator.getResult() % 1 == 0){
                resultF.setText(String.valueOf((int)calculator.getResult()));
            }
            else {
                resultF.setText(String.valueOf(calculator.getResult()));
            }
            history = "";
            text = "";
        }

        historF.setText(history);
    }
}
